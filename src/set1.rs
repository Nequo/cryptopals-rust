fn hex2base64(input: &str) -> String {
    let hexstring = hex::decode(input).unwrap();
    let base64string = base64::encode(hexstring);
    base64string
}

pub fn challenge1() {
    println!("Challenge1\n----------");
    let input = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
    println!("Hex: {:?}", input);
    let output = hex2base64(input);
    println!("Base64: {:?}", output);
    println!("\n")
}

fn fixed_xor(input1: &str, input2: &str) -> String {
    // hex::decode will take a string and convert it to a bytes vector
    let a = hex::decode(input1).unwrap();
    let b = hex::decode(input2).unwrap();
    let xor: Vec<u8> = a.iter().zip(b).map(|(x, y)| x ^ y).collect();
    hex::encode(xor)
}

pub fn challenge2() {
    println!("Challenge2\n----------");
    let a = "0001";
    let b = "0000";
    let result = fixed_xor(a, b);
    println!("{:?}", result);
    println!("\n");
}

fn single_byte_xor(hex_ciphertext: &str, key: u8) -> String {
    let bytes_ciphertext = hex::decode(hex_ciphertext).unwrap();
    let bytes_plaintext: Vec<u8> = bytes_ciphertext.iter().map(|x| x ^ key).collect();
    let plaintext = String::from_utf8_lossy(&bytes_plaintext).into_owned();
    plaintext
}

// https://www3.nd.edu/~busiforc/handouts/cryptography/letterfrequencies.html
const LETTER_FREQUENCIES: [f64; 27] = [
    //A       B        C         D         E         F         G         H
    0.084966, 0.02072, 0.045388, 0.033844, 0.111607, 0.018121, 0.024705, 0.030034,
    //I       J        K         L         M         N         O         P
    0.084966, 0.02072, 0.045388, 0.033844, 0.111607, 0.018121, 0.024705, 0.030034,
    //Q       R        S         T         U         V         W         X
    0.084966, 0.02072, 0.045388, 0.033844, 0.111607, 0.018121, 0.024705, 0.030034,
    //Y       Z        Space
    0.084966, 0.02072, 0.19181,
];

fn count_letters(input: &str) -> [u32; 27] {
    let mut lettercount = [0_u32; 27];
    for c in input.chars() {
        match c {
            //a is 97 in ascii and we want it indexed at 0
            'a'..='z' => {
                lettercount[c as usize - 97] += 1;
            }
            //A is 65 in ascii and we want it indexed at 0
            'A'..='Z' => {
                lettercount[c as usize - 65] += 1;
            }
            ' ' => lettercount[26] += 1,
            _ => {}
        }
    }
    lettercount
}

fn calculate_string_score(input: &str) -> f64 {
    let lettercount = count_letters(input);
    let score = LETTER_FREQUENCIES
        .iter()
        .zip(lettercount)
        .map(|(&x, y)| x * y as f64)
        .sum();
    score
}

fn decode_single_byte_xor(hex_ciphertext: &str) -> String {
    let mut plaintext = String::new();
    let mut max_score: f64 = 0.0;
    for k in 0..=255 {
        let possible_plaintext = single_byte_xor(hex_ciphertext, k);
        let score = calculate_string_score(&possible_plaintext);
        if score > max_score {
            max_score = score;
            plaintext = possible_plaintext;
        }
    }
    plaintext
}

pub fn challenge3() {
    println!("Challenge3\n----------");
    let result = decode_single_byte_xor(
        "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736",
    );
    println!("{:?}", result);
    println!("\n");
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn challenge1() {
        let input = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
        let output = hex2base64(input);
        assert_eq!(
            output,
            "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"
        )
    }
    #[test]
    fn challenge2() {
        let a = "1c0111001f010100061a024b53535009181c";
        let b = "686974207468652062756c6c277320657965";
        let xor = "746865206b696420646f6e277420706c6179";
        assert_eq!(fixed_xor(a, b), xor)
    }
    #[test]
    fn challenge3() {
        let hex_ciphertext = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736";
        let plaintext = "Cooking MC's like a pound of bacon";
        assert_eq!(decode_single_byte_xor(hex_ciphertext), plaintext)
    }
}
